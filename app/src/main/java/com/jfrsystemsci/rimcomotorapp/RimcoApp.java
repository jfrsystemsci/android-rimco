package com.jfrsystemsci.rimcomotorapp;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by angebagui on 06/10/2016.
 */

public class RimcoApp extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
