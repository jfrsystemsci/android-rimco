package com.jfrsystemsci.rimcomotorapp.model;

import com.jfrsystemsci.rimcomotorapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 05/10/2016.
 */

public class Category {

    private int icon;
    private String title;

    public Category() {

    }

    public Category(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static List<Category> getAll(){
        final List<Category> list = new ArrayList<>();
        list.add(new Category(R.drawable.icon_voiture_particulier, "Voiture particulier"));
        list.add(new Category(R.drawable.icon_voiture_utilitaire, "Voiture Utilitaire"));
        list.add(new Category(R.drawable.icon_camoin, "Camion"));
        list.add(new Category(R.drawable.icon_chariot, "Chariot"));
        list.add(new Category(R.drawable.icon_bus, "Bus"));
        list.add(new Category(R.drawable.icon_bus, "Menu"));
        return list;
    }
}
