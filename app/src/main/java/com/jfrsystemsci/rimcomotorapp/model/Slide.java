package com.jfrsystemsci.rimcomotorapp.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 05/10/2016.
 */

public class Slide {

    public String url_1;
    public String url_2;
    public String url_3;
    public String url_4;

    public Slide(String url_1, String url_2, String url_3, String url_4) {
        this.url_1 = url_1;
        this.url_2 = url_2;
        this.url_3 = url_3;
        this.url_4 = url_4;
    }

    public static List<Slide> getSlides(){
        List<Slide> slides = new ArrayList<>();
        slides.add(new Slide("http://informagicteam.com/motors/images/carrousel/c50.png","http://informagicteam.com/motors/images/carrousel/w5cd.png","http://informagicteam.com/motors/images/carrousel/Howo-A7-6x4.png","http://informagicteam.com/motors/images/carrousel/Sinotruk-Howo-A7-4x2-env.png"));
        slides.add(new Slide("http://informagicteam.com/motors/images/carrousel/c30.png","http://informagicteam.com/motors/images/carrousel/h6.png","http://informagicteam.com/motors/images/carrousel/h5-t.png","http://informagicteam.com/motors/images/carrousel/h5.png"));
        return slides;
    }

    public static List<String> getUrls(){
        final List<String> urls = new ArrayList<>();
        urls.add("http://informagicteam.com/motors/images/slider1.jpg");
        urls.add("http://informagicteam.com/motors/images/slider2.jpg");
        urls.add("http://informagicteam.com/motors/images/slider3.jpg");
        return urls;
    }
}
