package com.jfrsystemsci.rimcomotorapp;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Scroller;
import android.widget.Toast;
import java.lang.reflect.Field;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.jfrsystemsci.rimcomotorapp.adpater.CategoryAdapter;
import com.jfrsystemsci.rimcomotorapp.adpater.OnItemClickListener;
import com.jfrsystemsci.rimcomotorapp.adpater.Slide2Adapter;
import com.jfrsystemsci.rimcomotorapp.adpater.SlideAdapter;
import com.jfrsystemsci.rimcomotorapp.model.Category;
import com.jfrsystemsci.rimcomotorapp.model.Slide;
import com.jfrsystemsci.rimcomotorapp.operations.ImageOps;
import com.jfrsystemsci.rimcomotorapp.operations.NetworkOperationEvent;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.PhotoService;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.VehiculeService;
import com.jfrsystemsci.rimcomotorapp.service.DataFetchService;
import com.jfrsystemsci.rimcomotorapp.service.PhotoFetchService;
import com.jfrsystemsci.rimcomotorapp.util.AppSharedPreferences;
import com.jfrsystemsci.rimcomotorapp.util.BusProvider;
import com.jfrsystemsci.rimcomotorapp.util.RetainedFragmentManager;
import com.jfrsystemsci.rimcomotorapp.util.ServiceResult;
import com.squareup.otto.Subscribe;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.concurrent.TimeUnit;

import istat.android.widget.view.Slider;
import me.relex.circleindicator.CircleIndicator;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity implements ServiceResult {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    //ImageView vehiculeParticulier;
    //ImageView vehiculeUtilitaire;
    //ImageView camion;
    //ImageView chariot;
    //ImageView bus;
    AppSharedPreferences mSharedPreferences;
    public static final String PREFS_FIRST_TIME = "com.jfrsystemsci.rimcomotors.PREFS_FIRST_TIME";
    private RecyclerView categoryRecyclerView;
    private CategoryAdapter categoryAdapter;

    //private ViewPager mViewPager;
    private SlideAdapter slideAdapter;
    private Slide2Adapter slide2Adapter;

    private static long SLIDE_DELAY = 3;
    private static int mCurrentSlidePosition = 0;
    private Subscription subscription;
    private  ViewPager smallViewPager;
    private Subscription subscription2;

    private int DEFAULT_IMAGE_MARG = 20;

    /**
     * Used to retain the ImageOps state between runtime configuration
     * changes.
     */
    protected final RetainedFragmentManager mRetainedFragmentManager =
            new RetainedFragmentManager(this.getFragmentManager(),
                    LOG_TAG);

    /**
     * Provides image-related operations.
     */
    private ImageOps mImageOps;

    private  Slider sliderView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();



        categoryAdapter = new CategoryAdapter();
        slideAdapter = new SlideAdapter();
        slide2Adapter = new Slide2Adapter();

        categoryRecyclerView = (RecyclerView)findViewById(R.id.categoryRecyclerView);
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(this,3,GridLayoutManager.VERTICAL,false));
        categoryRecyclerView.setAdapter(categoryAdapter);

        categoryAdapter.setCategories(Category.getAll());
        slide2Adapter.setSlides(Slide.getSlides());

       // mViewPager = (ViewPager)findViewById(R.id.slideViewPager);

        sliderView = (Slider)findViewById(R.id.slideView);
        sliderView.setImagePhathList(Slide.getUrls());
        //sliderView.setAutoResizable(true);
      /*  sliderView.setSpeed(3);
        sliderView.setNextSpeed(3000);
        sliderView.setAutoResizable(true);
        sliderView.setVelocity(3);
        sliderView.setLevel(0);*/

        smallViewPager = (ViewPager)findViewById(R.id.carView);
        smallViewPager.setAdapter(slide2Adapter);

        slideAdapter.setList(Slide.getUrls());
       // mViewPager.setAdapter(slideAdapter);
        //CircleIndicator circleIndicator = (CircleIndicator)findViewById(R.id.indicator);
        //circleIndicator.setViewPager(mViewPager);
/*
        try{
            Field mScroller = mViewPager.getClass().getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            Scroller scroll = new Scroller(this);
            Field scrollDuration = scroll.getClass().getDeclaredField("mDuration");
            scrollDuration.setAccessible(true);
            scrollDuration.set(scroll, -3000);
            mScroller.set(mViewPager, scroll);
        }catch (Exception e){
            Toast.makeText(this, "something happened", Toast.LENGTH_LONG).show();
        }*/




    Button mActionButton = (Button)findViewById(R.id.btnUpdate);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDialogFragment();
            }
        });
/*        vehiculeParticulier = (ImageView)findViewById(R.id.imageView5VoitureParticulier);
        vehiculeUtilitaire = (ImageView)findViewById(R.id.imageView4VoitrureUtilitaire);
        camion =             (ImageView)findViewById(R.id.imageView3Camion);
        chariot             = (ImageView)findViewById(R.id.imageView2ChariotElevateur);
        bus               =  (ImageView)findViewById(R.id.imageView2Bus);*/



        categoryAdapter.setOnItemClickListener(new OnItemClickListener<Category>() {
            @Override
            public void onItemClicked(Category object, int position) {
                final Intent intent = new Intent(MainActivity.this, VehiculeListActivity.class);
                if(position == 0){
                    intent.putExtra(Intent.EXTRA_TEXT, 1);

                }else if(position == 1){

                    intent.putExtra(Intent.EXTRA_TEXT, 2);


                }else if(position == 2){
                    intent.putExtra(Intent.EXTRA_TEXT, 3);

                }else if(position == 3){

                    intent.putExtra(Intent.EXTRA_TEXT, 4);


                }else if(position == 4){
                    intent.putExtra(Intent.EXTRA_TEXT, 5);


                }
                startActivity(intent);
            }
        });



        mSharedPreferences = new AppSharedPreferences(this);
        boolean isFirstTime = mSharedPreferences.getBoolean(PREFS_FIRST_TIME, false);
        if (isFirstTime){
            Toast.makeText(getApplication(), "Vous pouvez appuyer sur le bouton UPDATE pour mettre à jour les données", Toast.LENGTH_LONG).show();
        }else {
            startDataFetchService();
        }


        // Handle any configuration change.
        handleConfigurationChanges();



    }
/*
    private void animateSlide() {

        subscription =   Observable.interval(SLIDE_DELAY, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (mViewPager.getCurrentItem() == (slideAdapter.getCount() - 1)) {
                            mCurrentSlidePosition = 0;
                        } else {
                            mCurrentSlidePosition = mViewPager.getCurrentItem() + 1;

                        }
                        mViewPager.setCurrentItem(mCurrentSlidePosition);
                    }
                });
    }*/

    private void animateSmallSlide(){
        subscription2 =   Observable.interval(SLIDE_DELAY, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {

                        if (smallViewPager.getCurrentItem() == (slide2Adapter.getCount() - 1)) {

                            smallViewPager.setCurrentItem(0);
                        } else {
                           final int smallCurrentPosition = smallViewPager.getCurrentItem() + 1;
                            smallViewPager.setCurrentItem(smallCurrentPosition);

                        }

                    }
                });
    }

    public void startDataFetchService(){
        mSharedPreferences.putBoolean(PREFS_FIRST_TIME, true);
        startService(new Intent(this, DataFetchService.class));
    }

    private void startDialogFragment() {

        FragmentManager fm = getSupportFragmentManager();
        DownloadFragment dialog = new DownloadFragment();
        dialog.show(fm, "download");
    }
/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        BusProvider.getInstance().register(this);

        sliderView.start();

        boolean firstTime = mSharedPreferences.getBoolean(PREFS_FIRST_TIME, false);
        if (firstTime){
           Toast.makeText(this, R.string.second_time_message, Toast.LENGTH_LONG).show();
        }else {
            //first time
          //  startDialogFragment();

        }
      //  animateSlide();
        animateSmallSlide();

    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        BusProvider.getInstance().unregister(this);
        if(subscription!=null && subscription.isUnsubscribed())
            subscription.unsubscribe();
        if(subscription2!=null && subscription2.isUnsubscribed())
            subscription2.unsubscribe();
        sliderView.stop();

    }



    @Subscribe
    public void onNetworkOperation(NetworkOperationEvent event) {
        if (event.hasStarted()) {
           // Toast.makeText(this, event.getMessage(), Toast.LENGTH_LONG).show();
        } else if (event.hasFinishedOne()) {
             //hideProgressBar();
        } else if (event.hasFinishedAll()) {
                //hideProgressBar();
            if (event.hasTypeVehiculeOperation()){
                startService(new Intent(this, PhotoFetchService.class));
            }
            if (event.hasTypePhotoOperation()){
                 PhotoService photoService = new PhotoService(this);
                 VehiculeService vehiculeService = new VehiculeService(this);
                for (Vehicule vehicule: vehiculeService.getAll()){
                    if (vehicule.getSmallPic() !=null && vehicule.getLargePic() != null){
                        addUrl(vehicule.getSmallPic());
                        addUrl(vehicule.getLargePic());
                    }
                }
                for (Photo photo: photoService.getAll()){
                    addUrl(photo.getLargePic());
                }
                downloadImages();
            }

        } else if (event.hasFailed()) {
           // hideProgressBar();
            Toast.makeText(this, event.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
    /**
     * Handle hardware reconfigurations, such as rotating the display.
     */
    protected void handleConfigurationChanges() {
        // If this method returns true then this is the first time the
        // Activity has been created.
        if (mRetainedFragmentManager.firstTimeIn()) {
            Log.d(LOG_TAG,
                    "First time onCreate() call");

            // Create the ImageOps object one time.
            mImageOps = new ImageOps(this);

            // Store the ImageOps into the RetainedFragmentManager.
            mRetainedFragmentManager.put("IMAGE_OPS_STATE",
                    mImageOps);

        } else {
            Log.d(LOG_TAG,
                    "Second or subsequent onCreate() call");

            // The RetainedFragmentManager was previously initialized,
            // which means that a runtime configuration change
            // occured, so obtain the ImageOps object and inform it
            // that the runtime configuration change has completed.
            mImageOps =
                    mRetainedFragmentManager.get("IMAGE_OPS_STATE");
            mImageOps.onConfigurationChange(this);
        }
    }

    /**
     * Called by the Android Activity framework when the user preses
     * the "Download and Display Image(s)" button in the UI.
     *
     */
    public void downloadImages() {
        mImageOps.startDownloads();
    }

    /**
     * Add whatever URL has been entered into the text field if that
     * URL is valid when user presses the "Add URL" button in UI.
     */
    public void addUrl(String url) {
        mImageOps.addUrl(url);
    }

    /**
     * Delete the previously downloaded pictures and directories when
     * user presses the "Delete Downloaded Image(s)" button in the UI.
     */
    public void deleteDownloadedImages() {
        mImageOps.deleteDownloadedImages();
    }

    @Override
    public void onServiceResult(int requestCode, int resultCode, Bundle data) {
        // Handle the results.
        mImageOps.doResult(requestCode,
                resultCode,
                data);
    }









}
