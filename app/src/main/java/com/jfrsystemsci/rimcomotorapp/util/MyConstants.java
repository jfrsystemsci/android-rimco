package com.jfrsystemsci.rimcomotorapp.util;

import android.content.Context;

/**
 * Created by angebagui on 24/06/2015.
 */
public class MyConstants {
    public static final String BASE_API = "http://rimco.esy.es/rimco-api/public/";
    public static final String _LIST_URL = "list/";
    public static final String _MOTOR_URL_VIEW = "view/";
    public static final String _VEHICULE_BY_CATEGORIE = "vehicule/";
    public static final String _PHOTO_BY_VEHICULE = "photo/";

    public static final int PREFS_MODE = Context.MODE_PRIVATE;

    public static final String PREFS_NAME = "com.jfrsystemsci.rimcomotorapp.PREFS_NAME";

    public static final String PREFS_USER_NUMBER = "com.jfrsystemsci.rimcomotorapp.PREFS_USER_NUMBER";

    public static final String PREFS_COME_FROM_REGISTRATION = "com.jfrsystemsci.rimcomotorapp.PREFS_COME_FROM_REGISTRATION";

    public static final String PREFS_USER_TOKEN = "com.jfrsystemsci.rimcomotorapp.PREFS_USER_TOKEN";

    public interface MyDB {
        public final static String DATABASE_NAME = "rimcodb_prod.db";

        public final static int DATABASE_VERSION = 1;
    }
}
