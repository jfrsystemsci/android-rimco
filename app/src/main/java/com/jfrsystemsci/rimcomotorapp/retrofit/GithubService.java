package com.jfrsystemsci.rimcomotorapp.retrofit;

import com.jfrsystemsci.rimcomotorapp.gsonclass.PhotoResult;
import com.jfrsystemsci.rimcomotorapp.gsonclass.VehiculeResult;


import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by angebagui on 27/06/2015.
 */
public interface GithubService {
    @GET("/api/vehicule/{categorieId}")
    void getListVehiculeByCategorie(@Path("categorieId") String categorieId, Callback<VehiculeResult> callback);
    @GET("/api/photo/{vehiculeId}")
    void getPhotosByVehicule(@Path("vehiculeId") String vehiculeId, Callback<PhotoResult> callback);
    @GET("/api/vehicules")
    void getListVehicule(Callback<VehiculeResult> callback);
    @GET("/api/photos")
    void getListPhoto(Callback<PhotoResult> callback);
}
