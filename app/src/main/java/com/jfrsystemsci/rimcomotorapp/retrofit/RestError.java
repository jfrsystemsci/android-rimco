package com.jfrsystemsci.rimcomotorapp.retrofit;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by angebagui on 26/06/2015.
 *
 * {
 "message":"Email already exists.Please try another email.",
 "error":true,
 "status":409,
 }
 *
 *
 */
public class RestError {

    public String message;
    public Integer status;


    public RestError() {
    }

    public RestError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("message", message);
            jsonObject.put("status", status);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }


    }
}
