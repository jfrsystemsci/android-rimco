package com.jfrsystemsci.rimcomotorapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;

/**
 * Created by angebagui on 29/06/2015.
 */
public class DownloadFragment extends DialogFragment {
    private static final String LOG_TAG = DownloadFragment.class.getSimpleName();


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View v = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_dialog_download, null);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("Syncronisation au Serveur de RIMCO MOTOR")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ((MainActivity)getActivity()).startDataFetchService();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                         return;
                    }
                }).create();
    }
}
