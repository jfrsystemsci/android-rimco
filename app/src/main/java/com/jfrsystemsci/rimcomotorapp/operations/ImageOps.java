package com.jfrsystemsci.rimcomotorapp.operations;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;



import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jfrsystemsci.rimcomotorapp.MainActivity;
import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.service.DownloadImageService;
import com.jfrsystemsci.rimcomotorapp.util.ServiceResultHandler;
import com.jfrsystemsci.rimcomotorapp.util.Utils;

/**
 * This class provides all the image-related operations.
 */
public class ImageOps {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     * Image-related operations.
     */
    enum OperationType {
        /**
         * Download an image.
         */
        DOWNLOAD_IMAGE
    }

    /**
     * Used to enable garbage collection.
     */
    WeakReference<MainActivity> mActivity;
    	
    /**
     * EditText field for entering the desired URL to an image.
     */
    private EditText mUrlEditText;

    /**
     * Linear layout to store TextViews displaying URLs.
     */
    private LinearLayout mLinearLayout;

    /**
     * Display progress to the user.
     */
    protected ProgressDialog mLoadingProgressBar;

    /**
     * Stores an instance of ServiceResultHandler.
     */
    public Handler mServiceResultHandler = null;
        
    /**
     * Stores the running total number of images downloaded that must
     * be handled by ServiceResultHandler.
     */
    public int mNumImagesToHandle;
    
    /**
     * Stores the running total number of images that have been
     * handled by the ServiceResultHandler.
     */
    public int mNumImagesHandled;
    
    /**
     * Stores the directory to be used for all downloaded images.
     */
    public String mDirectoryPathname = null;
    
    /**
     * Array of Strings that represent the valid URLs that have
     * been entered.
     */
    public ArrayList<String> mUrlList;

    /**
     * Constructor.
     */
    public ImageOps(MainActivity activity) {
        // Initialize the WeakReference.
        mActivity = new WeakReference<>(activity);

        // Initialize the downloadHandler.
        mServiceResultHandler =
            new ServiceResultHandler(mActivity.get());
                
        // Create a timestamp that will be unique.
        final String timestamp =
            new SimpleDateFormat("yyyyMMdd'_'HHmm").format(new Date());

        String app_name = activity.getString(R.string.app_name);
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), app_name);

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return;
            }
        }


        // Use the timestamp to create a pathname for the directory
        // that stores downloaded images.
       // mDirectoryPathname = Environment.getExternalStoragePublicDirectory
            //(Environment.DIRECTORY_DCIM)
            //+ "/" + timestamp + "/";
        mDirectoryPathname = mediaStorageDir.getPath() + File.separator;
        
        // Initialize the list of URLs.
        mUrlList = new ArrayList<String>();

        // Finish the initialization steps.
       initializeViewFields();
        resetNonViewFields();
    }        

    /**
     * Initialize all the View fields.
     */
  private void initializeViewFields() {
        // Store the ProgressBar in a field for fast access.
        mLoadingProgressBar = new ProgressDialog(mActivity.get());
        mLoadingProgressBar.setMessage("Un instant, telechargement en cours...");
        mLoadingProgressBar.setIndeterminate(false);
        mLoadingProgressBar.setCancelable(false);


    }

    /**
     * Reset the non-view fields (e.g., URLs and counters) and
     * redisplay linear layout.
     */
    private void resetNonViewFields() {
        mUrlList.clear();
        mNumImagesHandled = 0;
        mNumImagesToHandle = 0;
        //displayUrls();
    }

    /**
     * Start all the downloads.
     */
    public void startDownloads() {
        // Hide the keyboard.
        //Utils.hideKeyboard(mActivity.get(),
          //      mUrlEditText.getWindowToken());

        if (mUrlList.isEmpty())
            Utils.showToast(mActivity.get(),
                            "no images provided");
        else {
            // Make the progress bar visible.
            mLoadingProgressBar.show();

            // Keep track of number of images to download that must be
            // displayed.
            mNumImagesToHandle = mUrlList.size();

            // Iterate over each URL and start the download.
            for (String urlString : mUrlList)
                startDownload(Uri.parse(urlString));
        }
    }

    /**
     * Start a download.
     */
    private void startDownload(Uri url) {
        // Create an intent to download the image.
        Intent intent =
            DownloadImageService.makeIntent(mActivity.get(),
                                            OperationType.DOWNLOAD_IMAGE.ordinal(),
                                            url,
                                            mDirectoryPathname,
                                            mServiceResultHandler);
        Log.d(TAG,
              "starting the DownloadImageService for "
              + url.toString());

        // Start the service.
        mActivity.get().startService(intent);
    }

    /**
     * Handle the results returned from the Service.
     */
    public void doResult(int requestCode,
                         int resultCode,
                         Bundle data) {
        // Increment the number of images handled regardless of
        // whether this result succeeded or failed to download and
        // image.
        ++mNumImagesHandled;

        if (resultCode == Activity.RESULT_CANCELED) 
            // Handle a failed download.
            handleDownloadFailure(data);
        else { /* resultCode == Activity.RESULT_OK) */
            // Handle a successful download.
            Log.d(TAG,
                    "received image at URI "
                            + DownloadImageService.getImagePathname(data));
            mLoadingProgressBar.setMessage("Images téléchargées "+mNumImagesHandled+"/"+mNumImagesToHandle);
        }
                
        // Try to display all images received successfully.
       tryToDisplayImages(data);
    }

    /**
     * Launch an Activity to display all the images that were received
     * successfully if all downloads are complete.
     */
    public void tryToDisplayImages(Bundle data) {
        // If this is last image handled, display images via
        // DisplayImagesActivity.
        if (allDownloadsComplete()) {
            // Dismiss the progress bar.
            mLoadingProgressBar.dismiss();
            Toast.makeText(mActivity.get(), mNumImagesHandled+" images ont ete telechergee", Toast.LENGTH_LONG).show();
            // Initialize state for the next run.
            resetNonViewFields();


        }


    }

    /**
     * Handle failure to download an image.
     */
    public void handleDownloadFailure(Bundle data) {
        // Extract the URL from the message.
        final String url =
            DownloadImageService.getImageURL(data);
            
        Utils.showToast(mActivity.get(),
                        "image at " 
                        + url
                        + " failed to download!");

        // Remove the URL that failed from the UI.
        removeUrl(url);

        if (allDownloadsComplete()) {
            // Dismiss the progress bar.
            mLoadingProgressBar.dismiss();

            Toast.makeText(mActivity.get(), mNumImagesHandled+" images ont ete telechergee", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Returns true if all the downloads have completed, else false.
     */
    public boolean allDownloadsComplete() {
        return mNumImagesHandled == mNumImagesToHandle
            && mNumImagesHandled > 0;
    }

    /**
     * Returns true if there are any downloads in progress, else false.
     */
    public boolean downloadsInProgress() {
        return mNumImagesToHandle > 0;
    }

   /**
     * Add whatever URL has been entered into the text field if that
     * URL is valid when user presses the "Add URL" button in UI.
     */
    public void addUrl(String url) {
        // Get the user input (if any).


        if (URLUtil.isValidUrl(url)) {
            // Add valid URL to running list for download.
            mUrlList.add(url);

            // (Re)display all the URLs.
           // displayUrls();
    	} else
            Utils.showToast(mActivity.get(),
                            "Invalid URL"
                            + url);
    }

    /**
     * Remove a URL that couldn't be downloaded.
     */
    public void removeUrl(String url) {
        // Check if passed URL is in the list of URLs.
        if (mUrlList.contains(url)) {
            // Remove the invalid URL from the list.
            mUrlList.remove(url);
        } else {
            // Warn caller that URL was not in the list.
            Log.w(TAG, "RemoveUrl() - passed URL ("
                    + (url == null ? "null" : url.toString())
                    + ") is not in URL list.");
        }

        // If there are no more downloads pending dismiss the progress
        // bar.
       if (allDownloadsComplete())
           mLoadingProgressBar.dismiss();

        // (Re)display the URLs provided by the user thus far.
       // displayUrls();
    }



    /**
     * Delete all the downloaded images.
     */
    public void deleteDownloadedImages() {
        // Delete all the downloaded image.
        int fileCount = deleteFiles(mDirectoryPathname, 
                                    0);

        // Indicate how many files were deleted.
        Utils.showToast(mActivity.get(),
                        fileCount
                        + " downloaded image"
                        + (fileCount == 1 ? " was" : "s were")
                        + " deleted.");

        // Reset the non-view fields for the next run.
       // resetNonViewFields();
    }

    /**
     * A helper method that recursively deletes files in a specified
     * directory.
     */
    private Integer deleteFiles(String directoryPathname,
                                int fileCount) {
        File imageDirectory = new File(directoryPathname);        
        File files[] = imageDirectory.listFiles();

        if (files == null) 
            return fileCount;

        // Android does not allow you to delete a directory with child
        // files, so we need to write code that handles this
        // recursively.
        for (File f : files) {          
            if (f.isDirectory()) 
                fileCount += deleteFiles(f.toString(), 
                                         fileCount);
            Log.d(TAG,
                  "deleting file "
                  + f.toString()
                  + " with count "
                  + fileCount);
            ++fileCount;
            f.delete();
        }

        imageDirectory.delete();
        return fileCount;
    }

    /**
     * Called by the ImageOps constructor and after a runtime
     * configuration change occurs to finish the initialization steps.
     */
    public void onConfigurationChange(MainActivity activity) {
        // Reset the mActivity WeakReference.
        mActivity = new WeakReference<>(activity);

        // If we have a currently active service result handler, allow
        // the handler to update its outdated weak reference to the
        // ServiceResult callback implementation instance.
        if (mServiceResultHandler != null) {
            ((ServiceResultHandler)mServiceResultHandler)
                .onConfigurationChange(mActivity.get());
        }

        // (Re)initialize all the View fields.
          initializeViewFields();

        // If the content is non-null then we're done, so set the
        // result of the Activity and finish it.
        if (allDownloadsComplete()) {
            // Hide the progress bar.
          mLoadingProgressBar.dismiss();
            Log.d(TAG,
                  "All images have finished downloading");
            Toast.makeText(mActivity.get(), mNumImagesHandled+" images ont ete telechergee", Toast.LENGTH_LONG).show();
        } else if (downloadsInProgress()) {
            // Display the progress bar.
            mLoadingProgressBar.dismiss();

            Log.d(TAG,
                  "Not all images have finished downloading");
        }


    }
}
