package com.jfrsystemsci.rimcomotorapp.operations;

public class NetworkStartEvent {
    public boolean state;
    
    public NetworkStartEvent(boolean state){
        this.state = state;
    }
}