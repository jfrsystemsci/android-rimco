package com.jfrsystemsci.rimcomotorapp.adpater;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 05/10/2016.
 */

public class RecyclerViewVehiculeAdapter extends RecyclerView.Adapter<RecyclerViewVehiculeAdapter.ViewHolder> {

    private List<Vehicule> list;

    private OnItemClickListener<Vehicule> onItemClickListener;

    public RecyclerViewVehiculeAdapter() {
        this.list = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicule_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Context ctx = holder.itemView.getContext();
        final Vehicule vehicule = getItem(position);
        String f_url = vehicule.getSmallPic();

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClicked(vehicule,position);
            }
        });

        if (f_url.length()!=0){

            String app_name = ctx.getString(R.string.app_name);

            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), app_name);


            String filename = f_url.substring(f_url.lastIndexOf('/') + 1, f_url.length());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    filename);
            if (mediaFile!=null){
                holder.imageView.setImageURI(Uri.fromFile(mediaFile));
            }

        }


        if (vehicule.getModel().length() !=0){
            holder.modelTextView.setText(vehicule.getModel());
        }
    }

    private Vehicule getItem(int position) {
        return list.get(position);
    }

    public List<Vehicule> getList() {
        return list;
    }

    public void setList(List<Vehicule> list) {
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public OnItemClickListener<Vehicule> getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener<Vehicule> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
         SimpleDraweeView imageView;
         TextView modelTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (SimpleDraweeView)itemView.findViewById(R.id.imageView3);
            modelTextView = (TextView)itemView.findViewById(R.id.textView);
        }
    }
}
