package com.jfrsystemsci.rimcomotorapp.adpater;

/**
 * Created by angebagui on 05/10/2016.
 */

public interface OnItemClickListener <T>{

    void onItemClicked(T object, int position);
}
