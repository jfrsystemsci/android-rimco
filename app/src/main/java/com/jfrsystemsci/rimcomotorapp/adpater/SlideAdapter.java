package com.jfrsystemsci.rimcomotorapp.adpater;

import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.jfrsystemsci.rimcomotorapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 05/10/2016.
 */

public class SlideAdapter extends PagerAdapter {

    private List<String> list;

    public SlideAdapter() {
        this.list = new ArrayList<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = LayoutInflater.from(container.getContext()).inflate(R.layout.slide_layout,container,false);
        SimpleDraweeView draweeView = (SimpleDraweeView) view.findViewById(R.id.slideImageView);
        String url  = list.get(position);
        draweeView.setImageURI(Uri.parse(url), container.getContext());
            container.addView(view);
        return view;
    }

    public void setList(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        View view = (View) object;
        container.removeView(view);
    }
}
