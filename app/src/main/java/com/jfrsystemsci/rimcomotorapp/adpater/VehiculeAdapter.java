package com.jfrsystemsci.rimcomotorapp.adpater;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import android.widget.TextView;

import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.util.List;

/**
 * Created by angebagui on 26/06/2015.
 */
public class VehiculeAdapter extends ArrayAdapter<Vehicule> {

    LayoutInflater inflater ;
    Context ctx;
    public VehiculeAdapter(Context context, List<Vehicule> v) {
        super(context, 0, v);
        ctx = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null){
            convertView = LayoutInflater.from(ctx).inflate(R.layout.list_vehicule_item, parent, false);
        }

        SimpleDraweeView imageView = (SimpleDraweeView)convertView.findViewById(R.id.imageView3);
        TextView modelTextView = (TextView)convertView.findViewById(R.id.textView);



        Vehicule vehicule = getItem(position);
        String f_url = vehicule.getSmallPic();

        if (f_url.length()!=0){

            String app_name = ctx.getString(R.string.app_name);

            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), app_name);


            String filename = f_url.substring(f_url.lastIndexOf('/') + 1, f_url.length());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    filename);
                if (mediaFile!=null){
                    imageView.setImageURI(Uri.fromFile(mediaFile));
                }

        }


        if (vehicule.getModel().length() !=0){
            modelTextView.setText(vehicule.getModel());
        }



        return convertView;
    }
}
