package com.jfrsystemsci.rimcomotorapp.adpater;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.jfrsystemsci.rimcomotorapp.FullScreenViewActivity;
import com.jfrsystemsci.rimcomotorapp.R;

import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by angebagui on 26/06/2015.
 */
public class PhotoAdapter extends ArrayAdapter<Photo> {

    private Activity _activity;
    List<Photo> mPhotos;
    private String vehiculeId;
    public PhotoAdapter(Activity activity, List<Photo> mPhotos,  String vehiculeId) {
        super(activity, 0, mPhotos);
        this._activity = activity;
        this.mPhotos = mPhotos;
        this.vehiculeId = vehiculeId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = _activity.getLayoutInflater().inflate(R.layout.gallery_item, parent, false);
        }
        SimpleDraweeView imageView = (SimpleDraweeView)convertView.findViewById(R.id.gallery_item_imageView);

        Photo photo = getItem(position);

        String app_name = _activity.getString(R.string.app_name);

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), app_name);

        String f_url = photo.getLargePic();
        if (f_url !=null){

            String filename = f_url.substring(f_url.lastIndexOf('/') + 1, f_url.length());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    filename);


            if (mediaFile !=null){

                imageView.setImageURI(Uri.fromFile(mediaFile));

                // image view click listener
                imageView.setOnClickListener(new OnImageClickListener(position, vehiculeId));

            }


        }


        return convertView;
    }

    class OnImageClickListener implements View.OnClickListener {

        int _postion;
        String vehiculeId ;

        // constructor
        public OnImageClickListener(int position, String vehiculeId) {
            this._postion = position;
            this.vehiculeId = vehiculeId ;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
            Intent i = new Intent(_activity, FullScreenViewActivity.class);
            i.putExtra("position", _postion);
            i.putExtra("parentId", vehiculeId);
            _activity.startActivity(i);
        }

    }

    /*
     * Resizing image size
     */
    public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
        try {

            File f = new File(filePath);

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                    && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
