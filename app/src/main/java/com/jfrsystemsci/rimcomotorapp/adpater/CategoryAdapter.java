package com.jfrsystemsci.rimcomotorapp.adpater;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 05/10/2016.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{

    private List<Category> categories;

    private OnItemClickListener<Category> onItemClickListener;

    public CategoryAdapter() {
        this.categories = new ArrayList<Category>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Category category = categories.get(position);

        holder.titleTextView.setText(category.getTitle());
        holder.iconCategoryView.setImageResource(category.getIcon());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClicked(category,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener<Category> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView iconCategoryView;
        TextView titleTextView;
        public ViewHolder(View itemView) {
            super(itemView);
            iconCategoryView = (ImageView)itemView.findViewById(R.id.categoryIconView);
            titleTextView = (TextView)itemView.findViewById(R.id.categoryTitleView);
        }
    }
}
