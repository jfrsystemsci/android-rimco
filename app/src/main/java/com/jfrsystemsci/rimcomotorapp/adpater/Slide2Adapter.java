package com.jfrsystemsci.rimcomotorapp.adpater;

import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.model.Slide;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angebagui on 07/10/2016.
 */

public class Slide2Adapter extends PagerAdapter {

    private List<Slide> slides;

    public Slide2Adapter() {
        this.slides = new ArrayList<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = LayoutInflater.from(container.getContext()).inflate(R.layout.slide_layout_2,container,false);
        ViewHolder holder =  new ViewHolder(view);
        Slide slide = slides.get(position);
        Picasso.with(view.getContext()).load(slide.url_1).into(holder.slideView1);
        Picasso.with(view.getContext()).load(slide.url_2).into(holder.slideView2);
        Picasso.with(view.getContext()).load(slide.url_3).into(holder.slideView3);
        Picasso.with(view.getContext()).load(slide.url_4).into(holder.slideView4);
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return slides.size();
    }

    public List<Slide> getSlides() {
        return slides;
    }

    public void setSlides(List<Slide> slides) {
        this.slides = slides;
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        View view = (View) object;
        container.removeView(view);
    }

    public class  ViewHolder {
        public ImageView slideView1, slideView2,slideView3,slideView4;

        public View itemView;
        public ViewHolder(View itemView) {
            this.itemView = itemView;
            slideView1 = (ImageView)this.itemView.findViewById(R.id.slideView1);
            slideView2 = (ImageView)this.itemView.findViewById(R.id.slideView2);
            slideView3 = (ImageView)this.itemView.findViewById(R.id.slideView3);
            slideView4 = (ImageView)this.itemView.findViewById(R.id.slideView4);
        }
    }

}
