package com.jfrsystemsci.rimcomotorapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.gsonclass.PhotoResult;
import com.jfrsystemsci.rimcomotorapp.gsonclass.VehiculeResult;
import com.jfrsystemsci.rimcomotorapp.operations.NetworkOperationEvent;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.PhotoService;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.VehiculeService;
import com.jfrsystemsci.rimcomotorapp.retrofit.RestClient;
import com.jfrsystemsci.rimcomotorapp.util.BusProvider;
import com.jfrsystemsci.rimcomotorapp.util.Utils;
import com.squareup.otto.Bus;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by angebagui on 27/06/2015.
 */
public class PhotoFetchService extends Service {
    private static final String LOG_TAG = PhotoFetchService.class.getSimpleName();

    VehiculeService mVehiculeService;
    PhotoService mPhotoService;
    private Context mContext;
    private Bus mBus;

    private boolean networkState;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "I have been created.");
        BusProvider.getInstance().register(this);
        mVehiculeService = new VehiculeService(this);
        mPhotoService = new PhotoService(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "I have started.");
        mContext = this;
        mBus = BusProvider.getInstance();
        if (Utils.isConnected(this)){
            mBus.post(new NetworkOperationEvent(NetworkOperationEvent.HAS_STARTED, getString(R.string.message_bootstrapping_data), NetworkOperationEvent.TYPE_PHOTO_OPERATION));

            fetchAllPhotos();
            mBus.post(new NetworkOperationEvent(NetworkOperationEvent.HAS_FINISHED_ALL,NetworkOperationEvent.TYPE_PHOTO_OPERATION));


        }else {
            Toast.makeText(this, R.string.error_no_connection, Toast.LENGTH_LONG).show();
        }


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "I have been destroyed.");
        BusProvider.getInstance().unregister(this);
        super.onDestroy();

    }

    private void  fetchAllPhotos(){

        RestClient.get().getListPhoto( new Callback<PhotoResult>() {
            @Override
            public void success(PhotoResult photoResult, Response response) {
                if (photoResult !=null && photoResult.photos!=null){

                    if (!photoResult.photos.isEmpty()){
                        mPhotoService.saveAllPhoto(photoResult.photos);

                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (error !=null){
                    Log.e(LOG_TAG,error.getMessage());
                    Log.e(LOG_TAG,error.getUrl());
                    Log.e(LOG_TAG,error.toString());
                }

            }
        });

    }


}
