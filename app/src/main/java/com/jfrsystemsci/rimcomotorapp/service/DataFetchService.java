package com.jfrsystemsci.rimcomotorapp.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.jfrsystemsci.rimcomotorapp.R;
import com.jfrsystemsci.rimcomotorapp.gsonclass.VehiculeResult;
import com.jfrsystemsci.rimcomotorapp.operations.NetworkOperationEvent;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.VehiculeService;
import com.jfrsystemsci.rimcomotorapp.retrofit.RestClient;
import com.jfrsystemsci.rimcomotorapp.util.BusProvider;
import com.jfrsystemsci.rimcomotorapp.util.Utils;
import com.squareup.otto.Bus;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by angebagui on 26/06/2015.
 */
public class DataFetchService extends Service {

    private static final String LOG_TAG = DataFetchService.class.getSimpleName();

    VehiculeService mVehiculeService;

    private Context mContext;
    private Bus mBus;

    private static boolean networkState = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i(LOG_TAG, "I have been created.");
        BusProvider.getInstance().register(this);
        mVehiculeService = new VehiculeService(this);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "I have started.");
        mContext = this;
        mBus = BusProvider.getInstance();
        if (Utils.isConnected(this)){
            mBus.post(new NetworkOperationEvent(NetworkOperationEvent.HAS_STARTED, getString(R.string.message_bootstrapping_data), NetworkOperationEvent.TYPE_VEHICULE_OPERATION));

           fetchAllVehicules();


            mBus.post(new NetworkOperationEvent(NetworkOperationEvent.HAS_FINISHED_ALL,NetworkOperationEvent.TYPE_VEHICULE_OPERATION));



        }else {
            Toast.makeText(this, R.string.error_no_connection, Toast.LENGTH_LONG).show();
        }


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "I have been destroyed.");
        BusProvider.getInstance().unregister(this);
        super.onDestroy();

    }

    private void fetchAllVehicules(){
        RestClient.get().getListVehicule( new Callback<VehiculeResult>() {
            @Override
            public void success(VehiculeResult vehiculeResult, Response response) {
                if (vehiculeResult != null && vehiculeResult.vehicules!=null ){

                    if (!vehiculeResult.vehicules.isEmpty()){

                        List<Vehicule> vehicules = vehiculeResult.vehicules;

                        for (Vehicule v: vehicules){
                            mVehiculeService.saveVehicule(v);
                        }


                    }



                }

            }

            @Override
            public void failure(RetrofitError error) {
                if (error !=null){
                    Log.e(LOG_TAG,error.getMessage());
                    Log.e(LOG_TAG,error.getUrl());
                    Log.e(LOG_TAG,error.toString());
                }


            }
        });

    }


}
