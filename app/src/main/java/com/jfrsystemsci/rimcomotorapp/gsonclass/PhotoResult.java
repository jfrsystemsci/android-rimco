package com.jfrsystemsci.rimcomotorapp.gsonclass;

import com.google.gson.annotations.SerializedName;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;

import java.util.List;

/**
 * Created by angebagui on 24/06/2015.
 */
public class PhotoResult {

    @SerializedName("status")
    public String status;
    @SerializedName("photos")
    public List<Photo> photos;
}
