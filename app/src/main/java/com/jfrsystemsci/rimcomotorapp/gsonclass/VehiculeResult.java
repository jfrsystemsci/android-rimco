package com.jfrsystemsci.rimcomotorapp.gsonclass;

import com.google.gson.annotations.SerializedName;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by angebagui on 24/06/2015.
 */
public class VehiculeResult {

    @SerializedName("status")
    public String status;
    @SerializedName("vehicules")
    public List<Vehicule> vehicules;

    public VehiculeResult(String status, List<Vehicule> vehicules){
        this.status = status;
        this.vehicules = vehicules;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Vehicule> getVehicules() {
        return vehicules;
    }

    public void setVehicules(List<Vehicule> vehicules) {
        this.vehicules = vehicules;
    }

    public String toJSON(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
            jsonObject.put("vehicules", vehicules);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
