package com.jfrsystemsci.rimcomotorapp;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.jfrsystemsci.rimcomotorapp.adpater.OnItemClickListener;
import com.jfrsystemsci.rimcomotorapp.adpater.RecyclerViewVehiculeAdapter;
import com.jfrsystemsci.rimcomotorapp.adpater.VehiculeAdapter;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.VehiculeService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class VehiculeListActivityFragment extends Fragment {

    private static final String LOG_TAG = VehiculeListActivityFragment.class.getSimpleName();
    int categorie = 0;
    List<Vehicule> vehiculeList;
    VehiculeService mVehiculeService;
   // VehiculeAdapter mAdapter;
    RecyclerViewVehiculeAdapter mAdapter;
   // GridView mGridView;
    RecyclerView mGridView;
    public VehiculeListActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehiculeService = new VehiculeService(getActivity());
        mAdapter = new RecyclerViewVehiculeAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_vehicule_list, container, false);
        Intent intent = getActivity().getIntent();
        Fresco.initialize(getActivity());

        int categorie = intent.getIntExtra(Intent.EXTRA_TEXT, 0);

        RelativeLayout relativeLayout = (RelativeLayout)layout.findViewById(R.id.listVehiculeLayout);
        Resources res = getResources();
        if (categorie==1){
            relativeLayout.setBackground(res.getDrawable(R.drawable.particulier_phone));
        }else if (categorie==2){
            relativeLayout.setBackground(res.getDrawable(R.drawable.utilitaire_phone));
        }else if (categorie==3){
            relativeLayout.setBackground(res.getDrawable(R.drawable.camion_phone));

        }else if (categorie==4){

            relativeLayout.setBackground(res.getDrawable(R.drawable.chariot_phone));
        }else if (categorie==5){
            relativeLayout.setBackground(res.getDrawable(R.drawable.bus_phone));

        }

        //mGridView = (GridView)layout.findViewById(R.id.gridView);
        mGridView = (RecyclerView) layout.findViewById(R.id.gridView);
        mGridView.setLayoutManager(new GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false));
        vehiculeList = mVehiculeService.getVehiculeByCategorie(String.valueOf(categorie));
        if (vehiculeList !=null){
            Log.d(LOG_TAG, vehiculeList.toString());
        }else{
            Log.d(LOG_TAG, "Aucun touver");
        }

        setupAdapter();

/*        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), VehiculeActivity.class);
                Vehicule vehicule = vehiculeList.get(position);
                intent.putExtra(VehiculeActivityFragment.EXTRA_VEHICULE,vehicule.getId());
                startActivity(intent);
            }
        });*/

        mAdapter.setOnItemClickListener(new OnItemClickListener<Vehicule>() {
            @Override
            public void onItemClicked(Vehicule vehicule, int position) {
                Intent intent = new Intent(getActivity(), VehiculeActivity.class);
                intent.putExtra(VehiculeActivityFragment.EXTRA_VEHICULE,vehicule.getId());
                startActivity(intent);
            }
        });
        return layout;
    }

    private void setupAdapter() {

        if (mGridView==null || getActivity()==null)return;

        if (vehiculeList!=null){
           // mAdapter = new VehiculeAdapter(getActivity(),vehiculeList);
            mAdapter.setList(vehiculeList);
            mGridView.setAdapter(mAdapter);

        }else {
            mGridView.setAdapter(null);
        }
    }


}
