package com.jfrsystemsci.rimcomotorapp;

import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.jfrsystemsci.rimcomotorapp.ui.TwoWayGridView;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.jfrsystemsci.rimcomotorapp.adpater.PhotoAdapter;
import com.jfrsystemsci.rimcomotorapp.gallery.AppConstant;
import com.jfrsystemsci.rimcomotorapp.gallery.Utils;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.PhotoService;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.VehiculeService;

import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class VehiculeActivityFragment extends Fragment {

    public static final String EXTRA_VEHICULE = "com.jfrsystemsci.rimcomotorapp.EXTRA_VEHICULE";
    Vehicule mVehicule;
    VehiculeService mVehiculeService;
    PhotoService mPhotoService;
    PhotoAdapter mAdapter;
    SimpleDraweeView imageView;
    TwoWayGridView twoWayGridView;
    private int columnWidth;
    private Utils utils;

    List<Photo> photoList;
    public VehiculeActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVehiculeService = new VehiculeService(getActivity());
        mPhotoService = new PhotoService(getActivity());
        utils = new Utils(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Fresco.initialize(getActivity());
        View layout = inflater.inflate(R.layout.fragment_vehicule, container, false);
        long id = getActivity().getIntent().getLongExtra(EXTRA_VEHICULE, 0);
        mVehicule = mVehiculeService.getVehiculeById(id);

        LinearLayout relativeLayout = (LinearLayout)layout.findViewById(R.id.vehicueLayout);
        FrameLayout bottomLayout = (FrameLayout)layout.findViewById(R.id.bottomLayout);
        Resources res = getResources();
        int categorie = Integer.valueOf(mVehicule.categorieId);

        if (categorie==1){
            relativeLayout.setBackground(res.getDrawable(R.drawable.particulier_phone));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.particulier));
        }else if (categorie==2){
            relativeLayout.setBackground(res.getDrawable(R.drawable.utilitaire_phone));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.utilitaire));
        }else if (categorie==3){
            relativeLayout.setBackground(res.getDrawable(R.drawable.camion_phone));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.camion));

        }else if (categorie==4){

            relativeLayout.setBackground(res.getDrawable(R.drawable.chariot_phone));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.chariot));
        }else if (categorie==5){
            relativeLayout.setBackground(res.getDrawable(R.drawable.bus_phone));
            bottomLayout.setBackgroundColor(getResources().getColor(R.color.bus));
        }

        imageView = (SimpleDraweeView)layout.findViewById(R.id.imageView4);
        twoWayGridView = (TwoWayGridView)layout.findViewById(R.id.gridView2);

        // Initilizing Grid View
       // InitilizeGridLayout();
        photoList =  mPhotoService.getAllByVehicule(mVehicule.id);

        String f_url = mVehicule.getLargePic();

        if (f_url.length()!=0){

            String app_name = getString(R.string.app_name);

            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), app_name);


            String filename = f_url.substring(f_url.lastIndexOf('/') + 1, f_url.length());
            File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    filename);
            if (mediaFile != null){
                imageView.setImageURI(Uri.fromFile(mediaFile));
            }


        }

        setupAdapter();

        return layout;
    }

    private void setupAdapter() {
        if (getActivity()==null ||  twoWayGridView==null)return;

        if (photoList !=null){
            mAdapter = new PhotoAdapter(getActivity(), photoList, mVehicule.id);
            twoWayGridView.setAdapter(mAdapter);
        }else {
            twoWayGridView.setAdapter(null);
        }
    }


}
