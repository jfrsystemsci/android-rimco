package com.jfrsystemsci.rimcomotorapp.ormlite.entity;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by angebagui on 24/06/2015.
 */
@DatabaseTable(tableName = "tb_photo")
public class Photo extends BaseColumn{

    @DatabaseField(index = true, columnName = "internalId", unique = true, canBeNull = false)
    @SerializedName("id")
    public String id;

    @DatabaseField(columnName = "small_pic")
    @SerializedName("smallPic")
    public String smallPic;

    @DatabaseField(columnName = "large_pic")
    @SerializedName("largePic")
    public String largePic;

    @DatabaseField(columnName = "vehicule_id")
    @SerializedName("idVehicule")
    public String vehiculeId;

    public String getLargePic() {
        return largePic;
    }

    public void setLargePic(String largePic) {
        this.largePic = largePic;
    }

    public String getVehiculeId() {
        return vehiculeId;
    }

    public void setVehiculeId(String vehiculeId) {
        this.vehiculeId = vehiculeId;
    }

    public String getSmallPic() {
        return smallPic;
    }

    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    public String getInternalId() {
        return id;
    }

    public void setInternalId(String internalId) {
        this.id = internalId;
    }
}
