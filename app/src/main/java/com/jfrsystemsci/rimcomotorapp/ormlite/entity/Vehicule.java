package com.jfrsystemsci.rimcomotorapp.ormlite.entity;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by angebagui on 24/06/2015.
 */
@DatabaseTable(tableName = "tb_vehicule")
public class Vehicule extends BaseColumn{

    @DatabaseField(index = true, columnName = "internalId", unique = true, canBeNull = false)
    @SerializedName("id")
    public String id;

    @DatabaseField(columnName = "categorie_id")
    @SerializedName("idCategorie")
    public String categorieId;

    @DatabaseField(columnName = "marque_id")
    @SerializedName("idMarque")
    public String marqueId;

    @DatabaseField(columnName = "large_pic")
    @SerializedName("largePic")
    public String largePic;

    @DatabaseField(columnName = "small_pic")
    @SerializedName("smallPic")
    public String smallPic;

    @DatabaseField(columnName = "model")
    @SerializedName("model")
    public String model;

    public String getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(String categorieId) {
        this.categorieId = categorieId;
    }

    public String getMarqueId() {
        return marqueId;
    }

    public void setMarqueId(String marqueId) {
        this.marqueId = marqueId;
    }

    public String getLargePic() {
        return largePic;
    }

    public void setLargePic(String largePic) {
        this.largePic = largePic;
    }

    public String getSmallPic() {
        return smallPic;
    }

    public void setSmallPic(String smallPic) {
        this.smallPic = smallPic;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String toJSON(){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("internalId", getId());
            jsonObject.put("id", id);
            jsonObject.put("categorie_id", categorieId);
            jsonObject.put("marque_id", marqueId);
            jsonObject.put("large_pic", largePic);
            jsonObject.put("small_pic", smallPic);
            jsonObject.put("model", model);

        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = null;
        }

        return jsonObject.toString();
    }
}
