package com.jfrsystemsci.rimcomotorapp.ormlite.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Vehicule;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by angebagui on 24/06/2015.
 */
public class VehiculeDao implements BaseDao<Vehicule> {

    private Context mContext;
    private Dao<Vehicule, Long> mUserLongDao;
    private DatabaseHelper mDatabaseHelper;

    public VehiculeDao(Context mContext) {
        this.mContext = mContext;
        try {
            this.mDatabaseHelper = DatabaseHelper.getHelper(mContext);
            this.mUserLongDao = mDatabaseHelper.getDao(Vehicule.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Does not exist, returns false
     *
     * @param internalId
     * @return
     */
    public boolean findByInternalId(String internalId) {
        boolean flag = false;
        try {
            List<Vehicule> vehicules = mUserLongDao.queryForEq("internalId", internalId);
            if (vehicules.size() > 0) {
                mUserLongDao.delete(vehicules);
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean save(final Vehicule entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.create(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean update(final Vehicule entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.update(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean delete(final Vehicule entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.delete(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;

    }

    @Override
    public List<Vehicule> findAll() {
        List<Vehicule> users = null;
        try {
            users = mUserLongDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Vehicule findOneById(final long id) {
        Vehicule user = null;
        try {
            user = mUserLongDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public long countAll() {
        long count = 0;
        try {
            count = mUserLongDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Stitching inquiry, paging query
     *
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public List<Vehicule> findAllByLimit(final long offset, final long limit) {
        List<Vehicule> query = null;
        try {
            query = mUserLongDao.queryBuilder().
                    orderBy("create_date", false).
                    offset(offset).
                    limit(limit).
                    query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }
    public List<Vehicule> findAllByCategorie(String categ) {
        List<Vehicule> query = null;
        try {
            query = mUserLongDao.queryForEq("categorie_id", categ);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

}
