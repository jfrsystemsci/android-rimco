package com.jfrsystemsci.rimcomotorapp.ormlite.service;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.AndroidDatabaseConnection;
import com.j256.ormlite.support.DatabaseConnection;
import com.jfrsystemsci.rimcomotorapp.ormlite.dao.DatabaseHelper;
import com.jfrsystemsci.rimcomotorapp.ormlite.dao.PhotoDao;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;


import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.List;

/**
 * Created by angebagui on 24/06/2015.
 */
public class PhotoService {

    private static final String LOG_TAG = PhotoService.class.getSimpleName();
    // 添加事物
    public DatabaseConnection connection;
    private PhotoDao mUserDao;
    private Context mContext;

    public PhotoService(Context mContext) {
        this.mContext = mContext;
        mUserDao = new PhotoDao(mContext);
        connection = new AndroidDatabaseConnection(
                DatabaseHelper.getHelper(mContext).getWritableDatabase(), true);
    }

    /**
     * 参考这个,其他加事务吧
     *
     * @param user
     * @return
     */
    public int savePhoto(Photo user) {
        int flag = -1;//保存失败
        Savepoint savepoint = null;
        try {
            boolean byUsername = mUserDao.findByInternalId(user.id);
            if (byUsername) {
                flag = 0;//用户名存在
                Log.d(LOG_TAG, "Already in");
                savepoint = connection.setSavePoint("save");
                if (mUserDao.save(user)) {
                    flag = 1;//保存成功
                    connection.commit(savepoint);
                    Log.d(LOG_TAG, "Saved");
                }

            } else {
                savepoint = connection.setSavePoint("save");
                if (mUserDao.save(user)) {
                    flag = 1;//保存成功
                    connection.commit(savepoint);
                    Log.d(LOG_TAG, "Saved");
                }
            }
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                connection.rollback(savepoint);
                Log.e(LOG_TAG, "Saving Error", e);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return flag;
    }

    public boolean updatePhoto(Photo user) {
        return mUserDao.update(user);
    }

    public boolean deleteVehicule(Photo user) {
        return mUserDao.delete(user);
    }

    public Photo getPhotoById(long id) {
        return mUserDao.findOneById(id);
    }

    public List<Photo> getAll() {
        List<Photo> users = mUserDao.findAll();
        return users;
    }

    public long countAll() {
        return mUserDao.countAll();
    }

    public List<Photo> getAllByLimit(long currentPage, long size) {
        return mUserDao.findAllByLimit(currentPage - 1, size);
    }
    public void saveAllPhoto(List<Photo> photos){
        for (Photo photo: photos){
            savePhoto(photo);
        }
    }

    public List<Photo> getAllByVehicule(String vehicule_id){
        return mUserDao.findAllByVehicule(vehicule_id);
    }

}
