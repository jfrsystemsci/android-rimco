package com.jfrsystemsci.rimcomotorapp.ormlite.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;


import java.sql.SQLException;
import java.util.List;

/**
 * Created by angebagui on 24/06/2015.
 */
public class PhotoDao implements BaseDao<Photo> {
    private Context mContext;
    private Dao<Photo, Long> mUserLongDao;
    private DatabaseHelper mDatabaseHelper;

    public PhotoDao(Context mContext) {
        this.mContext = mContext;
        try {
            this.mDatabaseHelper = DatabaseHelper.getHelper(mContext);
            this.mUserLongDao = mDatabaseHelper.getDao(Photo.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Does not exist, returns false
     *
     * @param internalId
     * @return
     */
    public boolean findByInternalId(String internalId) {
        boolean flag = false;
        try {
            List<Photo> vehicules = mUserLongDao.queryForEq("internalId", internalId);
            if (vehicules.size() > 0) {
                mUserLongDao.delete(vehicules);
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean save(final Photo entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.create(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean update(final Photo entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.update(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean delete(final Photo entity) {
        boolean flag = false;
        try {
            int result = mUserLongDao.delete(entity);
            if (result > 0) {
                flag = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return flag;

    }

    @Override
    public List<Photo> findAll() {
        List<Photo> users = null;
        try {
            users = mUserLongDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Photo findOneById(final long id) {
        Photo user = null;
        try {
            user = mUserLongDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public long countAll() {
        long count = 0;
        try {
            count = mUserLongDao.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Stitching inquiry, paging query
     *
     * @param offset
     * @param limit
     * @return
     */
    @Override
    public List<Photo> findAllByLimit(final long offset, final long limit) {
        List<Photo> query = null;
        try {
            query = mUserLongDao.queryBuilder().
                    orderBy("create_date", false).
                    offset(offset).
                    limit(limit).
                    query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }
    public List<Photo> findAllByVehicule(String vehicule_id) {
        List<Photo> query = null;
        try {
            query = mUserLongDao.queryForEq("vehicule_id", vehicule_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }
}
