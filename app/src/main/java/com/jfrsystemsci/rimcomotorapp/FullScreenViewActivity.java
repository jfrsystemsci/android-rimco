package com.jfrsystemsci.rimcomotorapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.jfrsystemsci.rimcomotorapp.adpater.FullScreenImageAdapter;
import com.jfrsystemsci.rimcomotorapp.gallery.Utils;
import com.jfrsystemsci.rimcomotorapp.ormlite.entity.Photo;
import com.jfrsystemsci.rimcomotorapp.ormlite.service.PhotoService;

import java.util.List;

public class FullScreenViewActivity extends Activity{

	private Utils utils;
	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	PhotoService mPhotoService;

	List<Photo> photoList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);
		Fresco.initialize(this);
		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);
		String vehiculeId = i.getStringExtra("parentId");

		mPhotoService = new PhotoService(this);

		photoList =  mPhotoService.getAllByVehicule(vehiculeId);


		viewPager = (ViewPager) findViewById(R.id.pager);

		utils = new Utils(getApplicationContext());



		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
				photoList);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);
	}
}
